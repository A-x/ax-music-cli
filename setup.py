#!/usr/bin/env python
# -*- coding: utf-8 -*-


from setuptools import setup, find_packages

setup(
    name='ax-music',
    version='0.0.1',
    packages=find_packages(),
    install_requires=[
        'requests',
        'BeautifulSoup4',
        'pycrypto',
        'future'
    ],

    entry_points={
        'console_scripts': [
            'ax-music = NEMbox:start'
        ],
    },

    license='MIT',
    author='Timm',
    author_email='i@ivvw.in',
    url='https://bitbucket.org/A-x/ax-music-cli',
    description='music 163 command line!',
    keywords=['music', 'netease', 'cli', 'player'],
)
