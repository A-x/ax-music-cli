#!/usr/bin/env python
# encoding: UTF-8
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import division
from __future__ import absolute_import
from builtins import str
from future import standard_library
standard_library.install_aliases()

import curses
import traceback
import argparse
import sys

from .menu import Menu
version = "潇哥励志版(0.0.1)"


def start():
    nembox_menu = Menu()
    try:
        nembox_menu.start_fork(version)
    except (OSError, TypeError, ValueError, KeyError):
        # clean up terminal while failed
        nembox_menu.screen.keypad(1)
        curses.echo()
        curses.nocbreak()
        curses.endwin()
        traceback.print_exc()


if __name__ == '__main__':
    start()

parser = argparse.ArgumentParser()
parser.add_argument("-v",
                    "--version",
                    help="显示版本信息",
                    action="store_true")
args = parser.parse_args()
if args.version:
    latest = Menu().check_version()
    curses.endwin()
    print('当前版本：【网易云音乐 | '  + version + '】')
    if latest != version:
        print('最新版本：【网易云音乐 | ' + str(latest))
    sys.exit()
